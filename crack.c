#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

const int MALLOC_SIZE = 50;



// Given a target hash, crack it. Return the matching
// password.

char * crackHash(char *target, char *dictionary)
{
    char *line = (char *) malloc(MALLOC_SIZE);
    FILE *dictionaryFile = fopen(dictionary, "r");
    while(fgets(line, MALLOC_SIZE, dictionaryFile))
    {
        char *nl = strchr(line, '\n');
        if(nl != NULL)
            *nl = '\0';
        char *hashedLine = md5(line, strlen(line));
        //compares to target
        if(strcmp(hashedLine, target) == 0)
        {
            return line;
            printf("%s: %s\n", hashedLine, line);
        }
    }
    fclose(dictionaryFile);
    return NULL;
}



int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    // Open the hash file for reading.
    FILE *hash_file = fopen(argv[1], "r");
    
    char hash_file_line[512];
    for(int i = 0; i < 512; i++)
    {
        hash_file_line[i] = '\0';
    }

    // For each hash, crack it by passing it to crackHash
    while(fgets(hash_file_line, sizeof(hash_file_line), hash_file) != NULL)
    {
        //removes newline
        char *nl = strchr(hash_file_line, '\n');
            if(nl != NULL)
            {
                *nl = '\0';
            }
        char *crackResult = crackHash(hash_file_line, argv[2]);
        printf("%s: %s\n", hash_file_line, crackResult);
        free(crackResult);
    }
    // Close the hash file
    fclose(hash_file);
}
